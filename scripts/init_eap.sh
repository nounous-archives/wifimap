#!/bin/sh
# Initialize Cr@ns ssid with radius key

export PATH=/bin:/sbin:/usr/bin:/usr/sbin

server="138.231.148.11"
key=""

[ -z "$key" ] && {
  echo "Veuillez d'abord écrire le mot de passe dans le fichier !"
  exit 1
}

idx=0
while true; do
    local prefix=wireless.@wifi-iface[$idx]
    uci get $prefix -q > /dev/null || break
    [ "`uci get $prefix.network -q`" = crans ] && {
        echo Updating ssid $(uci get $prefix.ssid)
        uci set $prefix.key="$key"
        uci set $prefix.server="$server"
        uci set $prefix.disabled=0
    }
    idx=$(($idx+1))
done;
uci commit
wifi up
