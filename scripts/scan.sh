#!/bin/sh
# Ceci est le script envoyé au client pour récupérer
# un scan des ssid
#
# Copyright (C) 2012
# 
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/crans

SCAN_FACE=scanner
PHY=phy0
DEBUG=

run_cmd() {
    if [ -z $LOCAL ]; then
        debug "Running: $*"
        $*
    else
        debug "Would have run: $*"
    fi
}

debug() {
    if [ $DEBUG ]; then
        echo $*
    fi
}

enable_scan() {
    if [ -z $LOCAL ]; then
        vif_liste=`ls /var/run/hostapd-phy0`
        debug "vif liste loaded"
    else
        vif_liste="wlan0 wlan1"
        debug "Fake vif liste"
    fi
    run_cmd iw phy $PHY interface add $SCAN_FACE type managed
    for dev in $vif_liste; do
        run_cmd ifconfig $dev down
    done
    run_cmd ifconfig $SCAN_FACE up
    run_cmd ifconfig $SCAN_FACE down
    for dev in $vif_liste; do
        run_cmd ifconfig $dev up
    done
}


iw dev | grep -q $SCAN_FACE
if [ $? -eq 1 ]; then
    debug "SCAN_FACE missing, creating" 
    enable_scan
elif [ $? = 0 ]; then
    debug "Retour de grep inconnu: $?"
    exit $?
fi

run_cmd iwlist wlan0 scan

