#!/bin/sh
# Ceci est le script envoyé au client pour générer le
# fichier xml de status
#
# Copyright (C) 2012, 2015
# 
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/crans

[ -f /etc/wifimap ] && . /etc/wifimap

if test -f /etc/openwrt_version 
then

. /etc/openwrt_release

#LOCAL=0
PID_FILE=/var/run/status.pid
HOSTAPD_PATH=/var/run/hostapd

# Backward comp avec AA (mais mono-radio)
[ -d $HOSTAPD_PATH ] || {
    HOSTAPD_PATH=/var/run/hostapd-phy0
}

kill -9 $PID_FILE 2> /dev/null
echo -n $$ > $PID_FILE

raw_text()
{
    sed -e "s/</\&lt;/" -e "s/>/\&gt;/"
}

get_hostname()
{
  if [ -z $LOCAL ]
  then
    uci get system.@system[0].hostname
  else
    echo "localhost"
  fi
}

get_mac_iface()
{
  if [ -z $1 ]; then echo ""
  else ifconfig $1 | grep -oEi "([0-9a-f]{2}:){5}[0-9a-f]{2}" | tr 'A-F' 'a-f'
  fi
}

get_scan()
{
  if [ -z $LOCAL ]; then
    (iwlist wlan0 scanning | raw_text || echo "<error>Scan impossible</error>") 2> /dev/null 
  else
    echo "<error>Script local</error>"
  fi
}

get_channel()
{
  i=`echo $1 | sed -e 's/^wlan\(.+*\)\(-.*\)\?/\1/; t; d'`
  uci -p/var/run/state.pid get wireless.radio$i.channel
}

build_clients_parser()
{
    debut="s/^Station \(\([0-9a-f]\{2\}:\)\{5\}[0-9a-f]\{2\}\) (on \(.*\))$/"
    replace="        <client>\\n          <mac>\\1<\/mac>\\n          <interface>\\3<\/interface>/;"
    echo -n "1$debut$replace"
    echo -n "1!$debut        <\/client>\\n$replace"
    while [ -n "$1" ]
    do
        echo -n "s/^[\t\s]*$1:[\t\s]*\(.*\)$/          <$2>\\1<\\/$2>/; t end;"
        [ -z "$3" ] && break
        shift 2
    done
    echo -n "s/^[\t\s]*inactive time:[\t\s]*\(.*\) ms$/          <idle>\\1<\\/idle>/; t end;"
    echo -n "d; : end"
}

clients_parse="`build_clients_parser "tx bitrate" "down" "rx bitrate" "up" \
    "rx bytes" "down_count" "tx bytes" "up_count"\
    "signal avg" "attenuation" \
    "authorized" "authorized" \
    "authenticated" "authenticated"`"

clients_list()
{
    if [ -z $LOCAL ]; then iw dev $1 station dump; else cat examples/dump; fi
}

total_client=0
get_clients()
{
    ifaces=$*
    count=0
    for iface in $ifaces; do
        clients_list $iface | sed -e "$clients_parse"
        iface_c=`clients_list $iface | grep -c "^Station"`
        if [ ! $iface_c -eq 0 ]; then
            echo "        </client>"
        fi
        count=$(($count+$iface_c))
    done
    echo "        <client_count>$count</client_count>"
    total_client=$(($total_client+$count))
}

get_uptime()
{
    cat /proc/uptime | grep -o "^[^ ]*"
}


cat <<EOF
<borne>
    <raw fun="hostname"></raw>
    <localhostname>`get_hostname`</localhostname>
    <uptime>`get_uptime`</uptime>
    <mac>`get_mac_iface eth0`</mac>
    <localdate>`date +%s`</localdate>
    <uname>`uname -a`</uname>
    <release>`echo $DISTRIB_REVISION`</release>
    <raw fun="monitor"></raw>
    <raw fun="currdate"></raw>
    <raw fun="position"></raw>
    <raw fun="prise"></raw>
    <raw fun="comments"></raw>
    <raw fun="pontwifi"></raw>
    <raw fun="test"></raw>
    <editable />
    $EXTRA_XML
EOF

get_hostapd_config()
{
  hostapd_cli -p$HOSTAPD_PATH -i $1 get_config | sed "s/^$2=//;t;d"
}

SP='[\t\s]'
ifaces=$(iw dev | sed "s/^$SP*Interface \(.*\)$/\1/; t; d")
for iface in $ifaces; do
    IND="      "
    echo "    <network>"
    echo "$IND<interface>$iface</interface>"
    iw dev $iface info | sed "\
        s/^$SP*addr \(.*\)$/$IND<bssid>\1<\/bssid>/;\
        s/^$SP*ssid \(.*\)$/$IND<ssid>\1<\/ssid>/;\
        s/^$SP*channel \([0-9]*\).*$/$IND<channel>\1<\/channel>/;\
        t; d;\
    "
    get_clients $iface
    echo "    </network>"
done

echo "    <client_count>$total_client</client_count>"
#echo "    <raw fun=\"neighborhood\">`get_scan`</raw>"
echo "</borne>"


else

get_mac_iface()
{
  if [ -z $1 ]; then echo ""
  else ifconfig $1 | grep -oEi "([0-9a-f]{2}:){5}[0-9a-f]{2}" | tr 'A-F' 'a-f'
  fi
}

get_uptime()
{
    cat /proc/uptime | grep -o "^[^ ]*"
}

cat <<EOF
<borne>
    <raw fun="hostname"></raw>
    <localhostname>`cat /proc/sys/kernel/hostname`</localhostname>
    <uptime>`get_uptime`</uptime>
    <mac>`get_mac_iface ath0`</mac>
    <localdate>`date +%s`</localdate>
    <uname>`uname -a`</uname>
    <raw fun="monitor"></raw>
    <raw fun="currdate"></raw>
    <raw fun="position"></raw>
    <raw fun="prise"></raw>
    <raw fun="comments"></raw>
    <raw fun="pontwifi"></raw>
    <raw fun="test"></raw>
    <editable />
    $EXTRA_XML
EOF

get_ssid()
{
    grep "wireless.$1.ssid" /tmp/system.cfg | sed 's/wireless.*.ssid=//'
}

get_interface()
{
    grep "wireless.$1.devname" /tmp/system.cfg | sed 's/wireless.*.devname=//'
}

get_freq()
{
    iwlist ath0 channel | grep Current | awk '{print $5}' | sed 's/)//'
}

get_network()
{
cat <<EOF
    <network>
        <ssid>`get_ssid $1`</ssid>
        <bssid>`get_mac_iface ath0`</bssid>
        <channel>`get_freq $1`</channel>
        <interface>`get_interface $1`</interface>
EOF
        echo "     </network>"
}

for iface in `cat /tmp/system.cfg | grep wireless.[0-9].ssid= | sed 's/wireless.//' | cut -c 1`; do
    get_network $iface
done

total_client=`wlanconfig ath0 list | grep .*.:.*.:.*.:.*.:.*. -c`
echo "    <client_count>$total_client</client_count>"
#echo "    <raw fun=\"neighborhood\">`get_scan`</raw>"
echo "</borne>"

fi

