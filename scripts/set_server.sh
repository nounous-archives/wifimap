#!/bin/sh
#
# Copyright (C) 2012
# 
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/crans

RAD_SERVER=%(radius_server)s
RAD_KEY=%(radius_key)s

[ "`uci get wireless.@wifi-iface[0].server`" != "$RAD_SERVER" ] ||
[ "`uci get wireless.@wifi-iface[0].key`" != "$RAD_KEY" ] && {
  echo "reconfigured"
  uci set wireless.@wifi-iface[0].server=$RAD_SERVER
  uci set wireless.@wifi-iface[0].key=$RAD_KEY
  uci set wireless.@wifi-iface[0].disabled=0
  wifi
}

