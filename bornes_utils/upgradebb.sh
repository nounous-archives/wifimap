#!/bin/bash
echo "Nom de la borne:"
read Nom
ver=$(ssh -i /localhome/wifi_utils/apprenti_rsa root@$Nom.wifi.crans.org '. /lib/ar71xx.sh; ar71xx_board_name')
echo $ver
if [ $ver = "nanostation-m" ]
then
	ver="bullet-m"
        echo "nano renommée"
        echo $ver
fi
scp -i /localhome/wifi_utils/apprenti_rsa /localhome/wifi_utils/openwrt-barrier-breaker/bin/ar71xx/openwrt-ar71xx-generic-ubnt-$ver-squashfs-sysupgrade.bin root@$Nom.wifi.crans.org:/tmp
flash=/tmp/openwrt-ar71xx-generic-ubnt-$ver-squashfs-sysupgrade.bin
echo $flash
ssh -i /localhome/wifi_utils/apprenti_rsa root@$Nom.wifi.crans.org "sysupgrade -n ${flash}"
