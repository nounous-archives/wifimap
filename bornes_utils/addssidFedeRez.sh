#!/bin/bash
#Usage : ./addssidFederez borne clef_radius
echo "Attention, n'utilisez ce script que si vous savez ce que vous faites"
ssh $1.borne "echo '# federez
config interface vlan22
    option ifname   eth0.22
    option type     bridge
    option proto    none' >> /etc/config/network; echo '
config wifi-iface
        option device 'radio0'
        option network 'vlan22'
        option mode 'ap'
        option isolate '0'
        option wmm '1'
        option encryption 'wpa2'
        option server 'fda8:5d34:a228:c04:7261:6469:7573:3031'
        option dynamic_vlan '0'
        option ssid 'FedeRez'
        option key $2
        option disabled '0'' >> /etc/config/wireless; /etc/init.d/network restart"
echo "SSID federez ajoute"
