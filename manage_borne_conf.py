#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-
###############################################################################
# manage_borne_conf : backend de l'interface de configuration du parc de la
# wifi map
###############################################################################
# Author: Gabriel DETRAZ <detraz@crans.org>
# Copyright (C) 2015
# All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
###############################################################################

import os
import lc_ldap.shortcuts
import sys, getopt
import paramiko
import time

import socket
from gestion.config import liste_bats

# TODO: env de dev ?
_KEY_PATH = "/etc/crans/secrets/clefbornes.rsa"

def all_bornes(bats, ens=False, ip6only=False):
    """ Récupérer toutes les bornes wifi (objet ldap)"""
    liste = lc_ldap.shortcuts.lc_ldap_readonly().search(u'(objectClass=borneWifi)')
    def filtre(ap):
        if ip6only and 'ipHostNumber' in ap:
            return False
        for comment in ap['info']:
            if comment.value.startswith('test') or \
               comment.value.startswith('Pontwifi') or \
               comment.value.startswith('<'):
               return False
        if bats == 'all' or ens:
            batiments = liste_bats
        else:
            batiments = bats
        for b in batiments:
            try:
                if b.upper() in unicode(ap['prise'][0]) and not ens:
                    return True
                if b.lower() in unicode(ap['prise'][0]) and not ens:
                    return True
            except:
                if ens:
                    return True
    return filter(filtre,liste)

def all_hosts(bats, ens=False, okOnly=True, ip6only=False):
    """Renvoie la liste des noms des bornes"""
    host = lambda b: b['host'][0].value
    bornes = all_bornes(bats, ens)
    names = map(host, bornes)
    names.sort()
    return names

def exec_conn(bats, ens, cmd, borne=None):
    """Prend la liste des bornes, et execute la commande sur les n bornes"""
    pkey = paramiko.RSAKey.from_private_key_file(_KEY_PATH)
    if not borne:
        hosts = all_hosts(bats, ens, bats)
    else:
        hosts = [borne]
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    for host in hosts:
        print host
        for i in range(5):
            try:
                ssh.connect(host, username='root', pkey=pkey)
                stdin, stdout, stderr = ssh.exec_command(cmd)
                print stdout.readlines()
                ssh.close()
                break
            except (socket.error, paramiko.AuthenticationException, paramiko.SSHException):
                time.sleep(2)
                print u"Impossible de se connecter à " + unicode(host)
    return hosts

def __usage_brief(err):
    """Affiche un message d'erreur court"""

    print u"Option(s) invalide(s)" + err
    print u"Pour obtenir de l'aide sur l'utilisation de ce programme utilisez l'option -h"
    sys.exit(2)

def __usage():
    """Affichage de l'aide sur l'utilisation du script"""

    print u"""
    restartbornes.py
    Script permettant le redémarage de la diffusion des ssid (par defaut) ou le redémarage de /etc/init.d/network
    Il exclu normalement les ponts wifi (commentaire PontWifi)

    Usage : restartbornes.py OPTIONS

    options possibles :

    -b / --bats :
        Permet de spécifier les bâtiments à redémarrer.
        Incompatible avec l'option --all.
        ex : --bats=abh
    -h / --help :
        Affiche ce message d'aide.
    -n / --network :
        Rédémarre /etc/init.d/network
    -e / --ens :
        Inclu les bornes en zone ens
    -a / --all :
        Traite toutes les bornes excepté la zone ens

    Une des options --all ou --bats doit être obligatoirement spécifiée.
    """
    sys.exit(0)

# Traitement par défaut si le fichier est directement appellé comme un script
if __name__ == "__main__":
# Récupération des options
    if len(sys.argv) == 1:
    # Pas d'option fournie
        __usage_brief('')

    try:
        options, arg = getopt.getopt(sys.argv[1:],'b:hnea',['all','bats=','network','ens'])
    except getopt.error as msg :
        __usage_brief(unicode(msg))

    bats = ''
    cmd = 'wifi down; wifi up'
    ens = False
    network = False
    for opt, val in options:
        if opt == '-a' or opt == '--all':
            if bats:
                 __usage_brief(u"les options --all et --bats sont incompatibles !")
            bats = 'all'
        elif opt == '-b' or opt == '--bats':
            if bats == 'all':
                __usage_brief(u"les options --all et --bats sont incompatibles !")
            for c in val:
                if not (c in bats):
                    bats += c

        elif opt == '-h' or opt == '--help' :
            __usage()
        elif opt == '-e' or opt == '--ens' :
            ens = True
        elif opt == '-n' or opt == '--network' :
            cmd = '/etc/init.d/network restart'

    if bats != 'all':
        lbats = [bat for bat in bats]
        bats = lbats

    # On execute la fonction qui envoie les commandes ssh
    a =  exec_conn(bats, ens, cmd)
    print len(a)
    print a

