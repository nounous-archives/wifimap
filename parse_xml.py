# -*- coding: utf-8 -*-
###############################################################################
# parse_xml.py : récupération/traitement/affichage des données xml
###############################################################################
# Author: Daniel STAN <daniel.stan@crans.org>
# Copyright (C) 2012
# All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
###############################################################################

import xml.dom.minidom
import subprocess
import datetime
import os
import re
import requests

import ssh
import bornes
import gestion.secrets_new as secrets

import gestion.affich_tools as affich_tools
import lc_ldap.shortcuts

# Dossier où sont stockés les fichiers xml de status
STATUS_PATH = os.path.abspath(os.getenv('DBG_WIFIMAP_DB', '/var/lib/wifi_xml/'))
# Dossier pour les fichiers individuels
ALONE_PATH = os.path.join(STATUS_PATH, 'alone')
# Dossier des scripts
SCRIPTS_PATH = os.path.abspath(os.path.join(
    os.path.dirname(__file__), 'scripts'))
# URL où trouver le statut global
STATUS_URL = 'http://wifi-status.adm.crans.org/wifi-status.xml'

# Informations publiques (n'importe quel visiteur de la page peut les voir)
public_tags = ['borne','hostname','uptime','lon','lat','location',\
    'floor','network','ssid','bssid','channel','currdate',\
    'monitor','pontwifi', 'test'
    ]

# Informations accessibles qu'aux users logués
users_tags = public_tags + ['client_count']

# Informations privées (Nounou only)
# A priori pas utilisé (pour information seulement)
private_tags = users_tags + ['mac','uname','localhostname','localdate','currdate','comment',\
    'neighborhood','prise','client','raw','encryption','quality','level','other','error',\
    'release',\
    'idle','editable']

def init_db():
    """Initialise la db"""
    for path in [STATUS_PATH, ALONE_PATH]:
        if not os.path.isdir(path):
            os.makedirs(path)

@lc_ldap.shortcuts.with_ldap_conn(retries=2, delay=5,
    constructor=lc_ldap.shortcuts.lc_ldap_readonly)
def retrieve_xml(ldap=None, hosts=None, silent=False, timeout=5*60-30):
    """ Met à jour les xml des hosts donnés, et renvoie le tableau des erreurs """
    init_db()

    with open(os.path.join(SCRIPTS_PATH, 'status.sh'), 'r') as f:
        raw_code = f.read()
        code = raw_code
    if not hosts:
        hosts=bornes.all_hosts(fqdn=True)
        clean_up(hosts)
    results=ssh.sh_list(hosts,code,silent=silent,timeout=timeout)
    fails=[]
    if not silent:
        graph = affich_tools.anim('Traitement des réponses',len(hosts))
    for host in hosts:
        error = None
        if results['ret'][host] <> 0:
            error = "Bad answer from ssh (code %d) error output:\n%s" %\
                (results['ret'][host],results['err'][host])
        f=open(os.path.join(ALONE_PATH, '%s.xml' % host), 'w')
        borne = ldap.search('host=%s' % host)[0]
        try:
            if not error:
                doc=xml.dom.minidom.parseString(results['out'][host])
        except Exception as e:
            error = 'fail de parsing: %s' % str(e)
        if error:
            with open(os.path.join(SCRIPTS_PATH, 'offline.xml'), 'r') as errfile:
                doc=xml.dom.minidom.parse(errfile)
                errorTag=doc.createElement('error')
                errorTag.appendChild(doc.createTextNode(error))
                doc.documentElement.appendChild(errorTag)
        first_process(doc,host,borne)
        f.write(doc.toxml('utf-8'))
        f.close()
        silent or graph.cycle()

    return fails

def clean_up(hosts):
    """Efface les vieux xml, qui ne correspendent pas à des bornes dans la
    liste `hosts` fournie"""
    for dirname, dirnames, filenames in os.walk(ALONE_PATH):
        for fname in filenames:
            if not fname.endswith('.xml'):
                continue
            if fname[:-4] not in hosts:
                os.unlink(os.path.join(ALONE_PATH, fname))

def first_process(doc,host,borne):
    """ Processing avant mise en fichier """
    raw_items = doc.getElementsByTagName('raw')
    filter = Raw_filter(doc,borne)

    for raw in raw_items:
        if raw.hasAttribute('fun') and hasattr(filter,raw.getAttribute('fun')):
            fun = raw.getAttribute('fun')
        else:
            fun = 'default'
        if raw.firstChild:
            text = "".join([child.nodeValue for child in raw.childNodes\
                        if child.nodeType == child.TEXT_NODE])
        else:
            text = ''
        getattr(filter,fun)(text,raw)

def public_process(doc, allowed):
    """ Processing avant affichage public """
    def process(node):
        if node.nodeType <> node.ELEMENT_NODE:
            return
        elif node.tagName in allowed:
            for child in node.childNodes:
                process(child)
        else:
            node.parentNode.removeChild(node)
    process(doc)

def global_status(public=True, logged=True):
    """ Retourne un xml de toutes les bornes (données en cache)"""
    doc = xml.dom.minidom.parseString(requests.get(STATUS_URL).text.encode('utf-8'))
    for borne in doc.getElementsByTagName('borne'):
       if public and logged:
           allowed=users_tags
           public_process(borne, allowed)
       elif public:
           allowed=public_tags
           public_process(borne, allowed)

    return doc

class Raw_filter:
    """ Filtre et convertit les balises <raw> """
    def __init__(self,document,borne):
        self.document = document
        self.borne = borne

    def create_embeded_text(self,tag,text):
        """ Crée un noeud texte dans une balise <tag> """
        node = self.document.createElement(tag)
        node.appendChild(self.document.createTextNode(text))
        return node

    def monitor(self, text, node):
        new_node = self.document.createElement('monitor')
        host = self.borne['host'][0].value.encode(errors='ignore').split('.')[0]
        new_node.setAttribute('href',
                'http://munin.crans.org/wifi.crans.org/%s/index.html' % host)
        node.parentNode.replaceChild(new_node, node)

    def currdate(self,text,node):
        """ <raw fun="currdate"></raw> <currdate>Date courante</currdate> """
        dateText = self.document.createTextNode(datetime.datetime.now().strftime('%s'))
        dateNode = self.document.createElement('currdate')
        dateNode.appendChild(dateText)
        node.parentNode.replaceChild(dateNode,node)

    def comments(self,text,node):
        comments = self.borne['info']

        # Dans les conventions crans, le premier commentaire est le lieu
        if len(comments) > 0:
            v = comments[0]
            comments = comments[1:]
            node.parentNode.appendChild(self.create_embeded_text('location',v.value))

        for comment in comments:
            node.parentNode.appendChild(self.create_embeded_text('comment',comment.value))
        node.parentNode.removeChild(node)

    def position(self,text,node):
        x, y = None, None
        try:
            [y,x]=self.borne['positionBorne'][0].value.split(' ')
            float(x); float(y) # Au cas où la borne ait eu une position un jour
                               # puis N/A (cf except ValueError)
        except KeyError as e: # Position invalide
            node.parentNode.appendChild(self.create_embeded_text('error',\
                'Pas de position'))
        except IndexError: # aucune position dans la base ldap
            pass
        except ValueError: #Les coordonnées sont N/A (cast error sur float)
            pass

        if x and y:
            node.parentNode.appendChild(self.create_embeded_text('lon',x))
            node.parentNode.appendChild(self.create_embeded_text('lat',y))
        node.parentNode.removeChild(node)

    def hostname(self,text,node):

        host = self.borne['host'][0].value.split('.')[0]
        node.parentNode.replaceChild(self.create_embeded_text('hostname',\
            host),node)


    def prise(self,text,node):
        if not self.borne.get('prise', None):
            node.parentNode.removeChild(node)
        else:
            node.parentNode.replaceChild(self.create_embeded_text('prise',\
                self.borne['prise'][0].value.lower()),node)

    def mac(self,text,node):
        node.parentNode.replaceChild(self.create_embeded_text('mac',\
            self.borne['macAddress'][0].value.split('.')[0]),node)


    def pontwifi(self,text,node):
        if self.borne.get('pontwifi', [False])[0]:
            node.parentNode.appendChild(self.create_embeded_text('pontwifi',\
                'pontwifi'))

    def test(self,text,node):
        if self.borne.get('info', [None])[0]:
            if 'TEST' in self.borne.get('info', [None])[0].value:
                node.parentNode.appendChild(self.create_embeded_text('test',\
                'test'))

    def neighborhood(self,text,node):
        """Fonction moche qui parse un flux venant de iwlist pour en resortir
        la liste des cellules
        Exemple:
        print(voisinnage(ssh_exec('beli.wifi.crans.org','iwlist wl0 scanning')))"""
        ap = []
        neigh = self.document.createElement('neighborhood')
        curCell = None
        # Une cellule est un dictionnaire possédant les clés:
        # l'adresse mac (beware, l'adresse mac wifi)
        # channel, ssid, mode
        myFlags = re.IGNORECASE
        for l in text.split('\n'):
            l = l.strip()
            nMatch = re.match(
                '^Cell[^:]*:\s*(([0-9a-f]{2}:){5}[0-9a-f]{2})$',l,
            flags=myFlags)
            if nMatch:
                if curCell <> None:
                    neigh.appendChild(curCell)
                curCell = self.document.createElement('network')
                curCell.appendChild(self.create_embeded_text('bssid',nMatch.group(1).lower()))
            elif curCell <> None:
                mChannel = re.match('Channel\s*:\s*(\d*)$',l,
                    flags=myFlags)
                if mChannel:
                    curCell.appendChild(self.create_embeded_text('channel',mChannel.group(1)))
                    continue
                mSsid = re.match('essid:"([^"]*)"',l,flags=myFlags)
                if mSsid:
                    curCell.appendChild(self.create_embeded_text('ssid',mSsid.group(1)))
                    continue
                mMode = re.match('^Mode\s*:\s*(.*)$',l,flags=myFlags)
                if mMode:
                    curCell.appendChild(self.create_embeded_text('mode',mMode.group(1).lower()))
                    continue
                mEnc = re.match('^Encryption\s*key:(.*)$',l,flags=myFlags)
                if mEnc:
                    curCell.appendChild(self.create_embeded_text('encryption',mEnc.group(1).lower()))
                    continue
                mSignal = re.match('^Quality[:=](\d*)/(\d*)\s(?:.*)level[:=]-(\d*)\s*dBm',l,
                    flags=myFlags)
                if mSignal:
                    #curCell['quality']  =   float(mSignal.group(1))/float(mSignal.group(2))
                    curCell.appendChild(self.create_embeded_text('level',"-" + mSignal.group(3)))
                    continue

        if curCell <> None:
            neigh.appendChild(curCell)
        node.parentNode.replaceChild(neigh,node)

    def default(self,text,node):
        pass

def trace_mac(mac):
    """ Renvoie les hostsname où apparaît une mac """
    doc = global_status(public=False)
    mac = crans_utils.format_mac(mac).lower()
    res = []
    for tag in doc.getElementsByTagName('mac'):
        if not tag.firstChild or tag.firstChild.nodeValue <> mac:
            continue
        while tag.tagName <> 'borne':
            tag = tag.parentNode
        res.append(tag.getElementsByTagName('hostname')[0].firstChild.nodeValue)
    return res

