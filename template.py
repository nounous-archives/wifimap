#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-
###############################################################################
#template de commandes pour la propagation de la config des bornes sur intranet
###############################################################################
# Author: Gabriel DETRAZ <detraz@crans.org>
# Copyright (C) 2015
# All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
###############################################################################

hostapd='wifi down; wifi up'

network='/etc/init.d/network restart'

vlan='uci set network.vlan{0}=interface; uci set network.vlan{0}.ifname=eth0.{0}; uci set network.vlan{0}.type=bridge; uci set network.vlan{0}.proto=none; uci commit'

ssid_enable = 'uci set $(uci show wireless | grep {0} | cut -f 1-2 -d .).disabled=0; uci commit'

ssid_disable = 'uci set $(uci show wireless | grep {0} | cut -f 1-2 -d .).disabled=1; uci commit'

ssid = "uci add wireless wifi-iface; nbr=$(uci show wireless | tail -n 1 |  sed 's/^.*\[//' | sed 's/\].*//'); uci set wireless.@wifi-iface[$nbr].device=radio0; uci set wireless.@wifi-iface[$nbr].network=vlan{0}; uci set wireless.@wifi-iface[$nbr].mode=ap; uci set wireless.@wifi-iface[$nbr].isolate=0; uci set wireless.@wifi-iface[$nbr].wmm=1; uci set wireless.@wifi-iface[$nbr].encryption={1}; uci set wireless.@wifi-iface[$nbr].server={2}; uci set wireless.@wifi-iface[$nbr].ssid={3}; uci set wireless.@wifi-iface[$nbr].key={4}; uci set wireless.@wifi-iface[$nbr].disabled=0; uci commit"
