#!/bin/bash /usr/scripts/python.sh
# -*- encoding: utf-8 -*-

import sys
import os
import xml.dom.minidom

import parse_xml

MUNIN_CODE = 'iso-8859-15'

class MuninWifiPlugin(object):
    def __init__(self):
        fname = os.path.join(parse_xml.STATUS_PATH, 'wifi-status.xml')
        self.doc = xml.dom.minidom.parse(fname)

    def main(self, argv):
        if 'config' in argv:
            print self.config()
        elif 'autoconf' in argv:
            print 'yes'
        else:
            print self.output()

class PerAPPlugin(MuninWifiPlugin):
    def __init__(self):
        super(PerAPPlugin, self).__init__()
        self.data_config = {
            'title': 'fill it',
            'vlabel': 'vlabel fill it',
            'tagname': 'tag name of the value to graph',
            'draw': 'LINE1',
        }

    def config(self):
        conf = """graph_title %(title)s
graph_args --base 1000 -l 0
graph_vlabel %(vlabel)s
graph_category wifi""" % self.data_config

        hosts = [h.firstChild.nodeValue.encode(MUNIN_CODE) for h in
            self.doc.getElementsByTagName('hostname')]
        hosts.sort()

        # Le total est calculé à la volée à partir des autres valeurs
        return conf + '\n' + \
            '\n'.join(['%s.label %s\n%s.draw %s'\
              % (h, h, h, self.data_config['draw']) for h in hosts])# + \
            #'\ntotal.label total\ntotal.sum ' + ' '.join(hosts) + \
            #'\ntotal.draw LINE1'

    def value_for(self, borne):
        """Récupère le nœud ``borne`` et renvoie la valeur graphée"""
        for tag in borne.getElementsByTagName(self.data_config['tagname']):
            if tag.parentNode != borne:
                continue
            return tag.firstChild.nodeValue.encode(MUNIN_CODE)
        else:
            return '0'

    def output(self):
        out = []
        for borne in self.doc.getElementsByTagName('borne'):
            try:
                hostNode = borne.getElementsByTagName('hostname')[0].firstChild
            except KeyError:
                continue
            value = self.value_for(borne)
            out.append('%s.value %s' % (hostNode.nodeValue.encode(MUNIN_CODE),
                                        value))
        return '\n'.join(out)

class Clients(PerAPPlugin):
    def __init__(self):
        super(Clients, self).__init__()
        self.data_config.update({
            'title': 'Clients connectés',
            'vlabel': 'Machines',
            'tagname': 'client_count',
            'draw': 'AREASTACK',
        })

class Uptime(PerAPPlugin):
    def __init__(self):
        super(Uptime, self).__init__()
        self.data_config.update({
            'title': 'Uptime',
            'vlabel': 'Time',
            'tagname': 'uptime',
        })

class MeanIdle(PerAPPlugin):
    def __init__(self):
        super(MeanIdle, self).__init__()
        self.data_config.update({
            'title': 'Idle time moyen',
            'vlabel': 'Temps (s)',
            'tagname': 'idle',
        })
        # TODO: mettre un warning après 900secondes
        # le timeout par défaut d'hostapd est de 15min

    def value_for(self, borne):
        """Calcule le temps d'idle moyen de tous les clients"""
        total = 0
        num = 0
        for tag in borne.getElementsByTagName(self.data_config['tagname']):
            total += int(tag.firstChild.nodeValue)
            num += 1
        if num:
            return "%.0f" % (float(total)/num/1000)
        return "0"

class MeanAuth(PerAPPlugin):
    def __init__(self):
        super(MeanAuth, self).__init__()
        self.data_config.update({
            'title': "Nombre moyen d'auth",
            'vlabel': 'ratio',
            'tagname': 'authorized',
        })
        # TODO: mettre un warning

    def value_for(self, borne):
        """Calcule le nombre moyen de authorized"""
        total = 0
        num = 0
        for tag in borne.getElementsByTagName(self.data_config['tagname']):
            num += 1
            if tag.firstChild.nodeValue == 'yes':
                total += 1
        if num:
            return "%.0f" % (float(total)/num*100)
        return "100"

class On(MuninWifiPlugin):
    def config(self):
        return """graph_title Bornes allumées
graph_args --base 1000 -l 0
graph_vlabel Bornes
on.label Allumées
on.draw AREA
on.colour 00FF00
off.label Éteintes
off.draw STACK
off.colour FF0000
graph_category wifi"""

    def output(self):
        on = 0
        off = 0
        for tag in self.doc.getElementsByTagName('borne'):
            if tag.hasAttribute('offline'):
                off += 1
            else:
                on += 1
        return "on.value %d\noff.value %d" % (on,off)

class Channels(MuninWifiPlugin):
    #: canaux wifi autorisés par chez nous:
    # 1 à 13 (2,4Ghz) et 36 à 140 (par pas de 4)
    CHANNEL_RANGE = range(1,14) + [ x*4 for x in xrange(9,36) ]

    def config(self):
        out = """graph_title Répartition des canaux wifi
graph_args --base 1000 -l 0
graph_vlabel Bornes
graph_category wifi
"""
        for ch in self.CHANNEL_RANGE:
            out += """ch%d.draw AREASTACK\n""" % ch
            if ch > 16:
                band = "5Ghz"
            else:
                band = "2,4Ghz"
            out += """ch%d.label Canal %d (%s)\n""" % (ch, ch, band)
        return out

    def output(self):
        values = {ch: 0 for ch in self.CHANNEL_RANGE}
        for ap in self.doc.getElementsByTagName('borne'):
            try:
                chan_tag = ap.getElementsByTagName('channel')[0]
            except IndexError:
                continue
            try:
                values[int(chan_tag.firstChild.nodeValue)] += 1
            except KeyError:
                continue
        return "\n".join("ch%d.value %d" % item for item in values.iteritems())

if "wifi_clients" in sys.argv[0]:
    Clients().main(sys.argv)
elif "wifi_uptime" in sys.argv[0]:
    Uptime().main(sys.argv)
elif "wifi_channels" in sys.argv[0]:
    Channels().main(sys.argv)
elif "wifi_idle" in sys.argv[0]:
    MeanIdle().main(sys.argv)
elif "wifi_authorized" in sys.argv[0]:
    MeanAuth().main(sys.argv)
else:
    On().main(sys.argv)
