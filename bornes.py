# -*- coding: utf-8 -*-
###############################################################################
# bornes.py : Informations générales sur les bornes crans
###############################################################################
# Author: Daniel STAN <daniel.stan@crans.org>
# Copyright (C) 2012
# All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
###############################################################################

import os,sys
import lc_ldap.shortcuts

from socket import gethostname

use_ldap = lc_ldap.shortcuts.with_ldap_conn(retries=2, delay=5,
     constructor=lc_ldap.shortcuts.lc_ldap_readonly)
@use_ldap
def all_bornes(okOnly=True, ip6only=False, ldap=None):
    """ Récupérer toutes les bornes wifi (objet ldap)"""
    liste=ldap.search(u'(objectClass=borneWifi)')
    #liste.sort()
    if not okOnly:
        return liste
    def filtre(ap):
        if ip6only and 'ipHostNumber' in ap:
            return False
        if not ap.get('statut',[True])[0]:
            return False
        else:
            return True
    return filter(filtre,liste)

def all_hosts(okOnly=True, fqdn=False, ip6only=False):
    """Renvoie la liste des noms des bornes, sans le .wifi"""
    if fqdn:
        host = lambda b: b['host'][0].value
    else:
        host = lambda b: b['host'][0].value.split('.')[0]
    bornes = all_bornes(okOnly, ip6only)
    names = map(host, bornes)
    names.sort()
    return names

def nagios_genconf(root='/tmp/'):
    """Génère la conf pour nagios"""
    if not os.path.isdir(root):
        os.makedirs(root)
    f = file(root+'bornes.cfg','w')
    for b in all_bornes():
        address = b['host'][0].value
        host = address.split('.')[0]
        if b['prise']:
            prise = b['prise'][0].value
            parent = 'bat%s-%s' % (prise[0].lower(),prise[1])
        else:
            parent = 'gordon'
        model = 'openwrt-standard'
        f.write("""define host{
	host_name %s
	use %s
	address %s
	parents %s
}\n""" % (host,model,address,parent))

