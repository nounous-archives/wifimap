#!/bin/bash /usr/scripts/python.sh
# -*- encoding: utf-8 -*-

import sys
import os
import xml.dom.minidom

VERBOSE = False
if "-v" in sys.argv:
    VERBOSE = True

import parse_xml
parse_xml.retrieve_xml(silent=not VERBOSE)

with open(os.path.join(parse_xml.STATUS_PATH, 'wifi-status.xml'), 'w') as f:
    doc=xml.dom.minidom.Document()
    status=doc.createElement('status')
    doc.appendChild(status)
    for dirname, dirnames, filenames in os.walk(parse_xml.ALONE_PATH):
        for fname in filenames:
            borne=xml.dom.minidom.parse(os.path.join(parse_xml.ALONE_PATH, fname))
            status.appendChild(borne.documentElement)
    f.write(doc.toxml().encode('UTF-8'))
