#!/bin/bash /usr/scripts/python.sh
# -*- encoding: utf-8 -*-

""" Script pour générer un fichier d'aliases compatible avec
    l'appli Android Wifi Analyzer.

    On utilise le fichier XML généré par la Wifimap pour avoir les infos.

    NB: les bornes virtuelles (utilisées pendant la mise en place de la nouvelle
        couverture wifi) ne parasitent pas le résultats car elle n'ont pas de
        balise <network>

Author: Vincent Le Gallic <legallic@crans.org>
"""

import xml.dom.minidom
import urllib
xml_page = "https://intranet2.crans.org/wifimap/get_xml"

endline_char = "\r\n"
header = u"""#----------WifiAnalyzer alias file----------
#
#Encoding: UTF-8.
#The line starts with # is comment.
#
#Content line format:
#bssid1|alias of bssid1
#bssid2|alias of bssid2
#
"""

template_alias = u"%(ssid)s (%(host)s, %(where)s)"
template_line = u"%s|%s\n"

default_where = u"Nowhere"

import optparse
parser = optparse.OptionParser()

parser.add_option('-w', '--nowhere', help="Inclure les bornes sans location",
                  dest='include_nowhere', default=False, action='store_true')
parser.add_option('-i', '--input-file', help=u"Fichier d'alias pré-existants",
                  dest='input_file', type='string')
parser.add_option('-r', '--override', help=u"En cas de conflit, écraser les anciens alias",
                  dest='override', default=False, action='store_true')
parser.add_option('-o', '--output-file', help=u"Fichier cible",
                  dest='output_file', type='string')

def get_bornes():
    """Récupère la liste des bornes grâce à la wifimap"""
    page = urllib.urlopen(xml_page)
    obj = xml.dom.minidom.parse(page)
    bornes = obj.getElementsByTagName("borne")
    return bornes

def get_ssids(borne):
    """Récupère les paires SSID, BSSID"""
    networks = borne.getElementsByTagName("network")
    l = []
    for n in networks:
        paire = []
        try:
            for field in ["ssid", "bssid"]:
                paire.append(n.getElementsByTagName(field)[0].firstChild.data)
            l.append(paire)
        except AttributeError: pass
    return l

def get_host(borne):
    """Récupère le hostname de la borne"""
    hostnames = borne.getElementsByTagName("hostname")
    try:
        return hostnames[0].firstChild.data
    except:
        raise TypeError(u"Cette borne n'a pas d'hostname" % borne.toxml())

def get_where(borne):
    """Renvoie les informations de lieu sur la borne"""
    locations = borne.getElementsByTagName("location")
    try:
        return locations[0].firstChild.data
    except IndexError:
        return default_where

def get_aliases(bornes, include_nowhere):
    """Traduit les infos pour toutes les bornes en des dicos d'infos suffisantes"""
    aliases = []
    for b in bornes:
        info = {u'host' : get_host(b),
                u'where' : get_where(b)}
        networks = get_ssids(b)
        if info[u"where"] != u"Nowhere" or include_nowhere:
            for (ssid, bssid) in networks:
                alias = {u'host' : info[u'host'],
                         u'where' : info[u'where'],
                         u'ssid' : ssid,
                         u'bssid' : bssid}
                aliases.append(alias)
    aliases.sort(lambda x,y: cmp( (x[u"ssid"], x[u"host"]), (y[u"ssid"], y[u"host"])) )
    return aliases

def compute_aliases(aliases, previous={}, override=False):
    """Donne la liste des alias à partir de la forme dico, à partir d'une liste d'alias pré-existants
       override = True signifie que nos alias écrasent les pré-existants"""
    # On calcule nos alias
    aliases_dico = {}
    for a in aliases:
        aliases_dico[a[u"bssid"]] = template_alias % a
    # On override dans le bon sens
    if override:
        previous.update(aliases_dico)
        out = previous
    else:
        aliases_dico.update(previous)
        out = aliases_dico
    # On les formate
    liste = out.items()
    liste.sort(lambda x,y: cmp(x[1],y[1]))
    out = [template_line % tuple(i) for i in liste]
    return out

def parse_aliasfile(aliasfilename):
    """Récupère une liste d'alias dans un fichier généré par WifiAnalyzer"""
    with open(aliasfilename) as f:
        l = [i for i in f.readlines() if not i.startswith("#")]
    aliases = {}
    for i in l:
        i = i.strip().split("|")
        aliases[i[0].decode("UTF-8")] = i[1].decode("UTF-8")
    return aliases
        

def do_it(opts):
    """Effectue toutes les opérations"""
    bornes = get_bornes()
    aliases = get_aliases(bornes, opts.include_nowhere)
    if opts.input_file:
        prealiases = parse_aliasfile(opts.input_file)
    else:
        prealiases = {}
    aliases = compute_aliases(aliases, prealiases, opts.override)
    output = header
    output += u"".join(aliases)
    return output


if __name__ == "__main__":
    (opts, args) = parser.parse_args()
    result = do_it(opts)
    if opts.output_file:
        with open(opts.output_file, 'w') as f:
            f.write(result.encode("UTF-8"))
    else:
        print result.replace(endline_char, "\n").encode("UTF-8")
