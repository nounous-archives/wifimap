#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-

"""
Script qui permet d'avoir les logs de toutes les bornes et les renvoie à thot.
Développé en raison de bugs avec logread et l'ipv6 only pour l'administration
des bornes.
Ecrit par Daniel Stan et Gabriel Détraz
<dstan@crans.org>    <detraz@crans.org>
"""

import time
import sys, os
import select
import socket
import collections
import logging
import logging.handlers
from datetime import datetime
from daemon import runner

import bornes
import ssh

#: Délai entre deux détections de mort de connexions
DELAY_FILTER = 30

#: Délai (en s) entre deux tentatives de reconnexions et refresh de la liste
# des bornes
DELAY_BORNE = 300

class App(object):
    def __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/null'
        self.stderr_path = '/dev/null'
        self.pidfile_path = '/var/run/getlogwifi.pid'
        self.pidfile_timeout = 5
        self.paramiko = None

        self.commands = (
            ('[ -f /tmp/log/messages ] && tail -f -n 0 /tmp/log/messages || logread -f | grep -v dropbear', self.process_log),
        )

        self.channels_of_client = collections.defaultdict(list)
        self.cb_of_channel = {}
        self.host_of_client = {}
        self.client_of_channel = {}
        self.to_connect = []

    def process_log(self, channel, line):
        """Renvoie les logs dans syslog"""
        self.logger.info(line)

    # On crée une nouvelle connexion, on éxecute la commande des logs, on ajoute
    #le channel crée à la liste des channels, le client à celle des clients.
    def new_connection(self, client, host):
        self.host_of_client[client] = host
        client.set_missing_host_key_policy(self.paramiko.AutoAddPolicy())
        k = self.paramiko.RSAKey.from_private_key_file(ssh.SSH_KEY)
        client.connect(hostname = host, username = 'root', pkey=k)
        for (cmd, fct) in self.commands:
            channel = client.get_transport().open_session()
            channel.exec_command(cmd)
            self.channels_of_client[client].append(channel)
            self.cb_of_channel[channel] = fct
            self.client_of_channel[channel] = client

    # Si le client est mort, on le retire de la liste des clients, on retire ses
    # channels de la liste des channels, on ajoute l'hôte correspondant à la
    # self.liste to_connect
    def lost_client(self, client):
        """Déclare (et prend les mesures nécessaires) lorsqu'une connexion
        à un client meurt"""
        print "%s is now dead" % self.host_of_client[client]
        host = self.host_of_client[client]
        try:
            del self.host_of_client[client]
        except KeyError:
            pass
        self.to_connect.append(host)

        for channel in self.channels_of_client[client]:
            del self.cb_of_channel[channel]
            del self.client_of_channel[channel]

        del self.channels_of_client[client]

    # On essaye de lancer/relancer la connexion, on fait appel à new_connection,
    # en cas d'erreur, on passe.
    def refresh_connections(self):
        hosts, self.to_connect = self.to_connect, []
        for host in hosts:
            try:
                client = self.paramiko.SSHClient()
                self.new_connection(client, host)
            except (socket.error, EOFError, IOError, self.paramiko.ChannelException):
                self.lost_client(client)
                print "failed to connect to %s" % host
            except self.paramiko.AuthenticationException:
                self.lost_client(client)
                print "failed to connect to %s" % host

    # On recence les clients morts, on essaye d'exécuter la commande true dans chacuns d'eux,
    #si ca plante, on lance lost_client avec en argument le dit client.
    def filter_dead(self):
        """Teste les clients connectés pour savoir si la connexion fonctionne
        toujours"""
        for client in self.channels_of_client.keys():
            try:
                client.exec_command('true')
            except (socket.error, EOFError, IOError, self.paramiko.ChannelException):
                self.lost_client(client)

    def init_logger(self):
        """Initialisation du logger. Basically, envoie tout à syslog
        qui sera centralisé ensuite"""
        self.logger = logging.getLogger('getlogwifi')
        self.logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(name)s: [%(levelname)s] %(message)s')
        handler = logging.handlers.SysLogHandler(address = '/dev/log')
        try:
            handler.addFormatter(formatter)
        except AttributeError:
            handler.formatter = formatter
        self.logger.addHandler(handler)
        self.logger.info("started !")

    # On obtient la liste des bornes, que l'on met à jour toutes les x minutes.
    # Tous les DELAY_FILTER, on compte les morts, on essaye de relancer tout
    # ceux qui sont dans lost_client.
    # Enfin, dès qu'un log arrive, on passe le select, on l'envoie en tant que
    # tel
    def run(self):
        import paramiko as paramiko_module
        self.paramiko = paramiko_module

        self.init_logger()

        self.to_connect = bornes.all_hosts(fqdn=True, ip6only=True)

        last_filter = int(time.time()) - DELAY_FILTER
        last_refresh = int(time.time()) - DELAY_BORNE
        while True:
            wait_filter = last_filter + DELAY_FILTER - int(time.time())
            wait_refresh = last_refresh + DELAY_BORNE - int(time.time())
            if wait_filter <= 0:
                self.filter_dead()
                self.refresh_connections()
                wait_filter = DELAY_FILTER
                last_filter = int(time.time())

            if wait_refresh <= 0:
                old_list = self.to_connect + self.host_of_client.values()
                fresh_list = bornes.all_hosts(fqdn=True, ip6only=True)
                old_list = set(old_list)
                fresh_list = set(fresh_list)
                for vieux in old_list.difference(fresh_list):
                    # On imagine que vieux est déjà une borne injoinable
                    if vieux in self.to_connect:
                        self.to_connect.remove(vieux)
                for jeune in fresh_list.difference(old_list):
                    self.to_connect.append(jeune)

                wait_refresh = DELAY_BORNE
                last_refresh = int(time.time())

            wait = min(wait_refresh, wait_filter)
            to_poll = self.cb_of_channel.keys()
            print "Polling for %ds in %d connections" % (wait, len(to_poll))
            sys.stdout.flush()
            to_read, _, _ = select.select(to_poll, [], [], wait)
            for channel in to_read:
                raw = channel.recv(2048)
                if raw == '':
                    self.lost_client(self.client_of_channel[channel])
                else:
                    for line in raw[:-1].split('\n'):
                        self.cb_of_channel[channel](channel, line)

if __name__ == '__main__':
    app = App()
    if '-f' not in sys.argv:
        daemon_runner = runner.DaemonRunner(app)
        daemon_runner.do_action()
    else:
        app.run()

