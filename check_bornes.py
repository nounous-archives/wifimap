#!/bin/bash /usr/scripts/python.sh
#-*- mode: python; coding: utf-8 -*-
#
# Auteur : Emmanuel Arrighi <arrighi@crans.org>
# Licence : GPLv3
# Date : 07/03/2016

"""
Contrôle automatique des bornes wifi Crans.
Liste les bornes wifi joignables qui n'ont pas fait d'authentification au serveur
radius depuis plus de 24h.

Ce script doit être lancé sur thot.
"""

## import de la lib standard
import os
import sys
import argparse
import gzip
import subprocess
import socket

## import ldap de /usr/scripts
import lc_ldap.shortcuts

#: Path des log d'authentification wifi
LOG_PATH = "/var/log/wifi/global.log"

def search_ldap(ldap):
    """Liste les ip des bornes wifi de la base LDAP."""
    resultats = ldap.search(u"(&(ipHostNumber=10.231.148.*)(!(info=*Pontwifi*))(!(info=<*))(objectClass=borneWifi))")
    return resultats

def test_bornes(lborne):
    """Filtre les ip qui répondent à un ping."""
    list_host = [ borne["host"][0].value for borne in lborne ]
    list_host_available = []
    with open(os.devnull, 'w') as devnull:
        resultat = subprocess.Popen([u"fping",u"-a"] + list_host , shell=False, stderr=devnull, stdout=subprocess.PIPE)
        list_host_available = resultat.stdout.read().splitlines()
        resultat = subprocess.Popen([u"fping6",u"-a"] + list_host , shell=False, stderr=devnull, stdout=subprocess.PIPE)
        list_host_available += [ host for host in resultat.stdout.read().splitlines() if not host in list_host_available]
    return [ borne for borne in lborne if borne["host"][0].value in list_host_available]

def verif_connexion(lborne):
    """Filtre les ip non présentes dans les log de connexion."""
    log = ""
    # Fichier de log wifi hardcodé
    with open(LOG_PATH, "r") as f:
        log = f.read().decode("ISO-8859-15")
    with gzip.open(LOG_PATH + ".1.gz", "r") as f:
        log += f.read().decode("ISO-8859-15")
    return [borne for borne in lborne if log.find(u"Nas: %s" % borne["ipHostNumber"][0].value.format().decode("ascii")) == -1]

if __name__ == "__main__":
    PARSER = argparse.ArgumentParser(description="Check si les bornes ont demandé une authentification d'une machine wifi dans les dernières 24h. Script à lancer sur thot.", add_help=False)
    PARSER.add_argument("--test", help="Se connecter à la base de test.", action="store_true")
    PARSER.add_argument('-h', '--help', help="Affiche ce message et quitte.", action="store_true")

    ARGS = PARSER.parse_args()

    if ARGS.help:
        PARSER.print_help()
        sys.exit(0)

    if socket.gethostname() != "thot":
        PARSER.print_help()
        sys.exit(1)

    if ARGS.test:
        LDAP = lc_ldap.shortcuts.lc_ldap_test()
    else:
        LDAP = lc_ldap.shortcuts.lc_ldap_readonly()

    for borne in verif_connexion(test_bornes(search_ldap(LDAP))):
        print borne
