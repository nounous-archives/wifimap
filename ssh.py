# -*- coding: utf-8 -*-
###############################################################################
# ssh.py : connexion ssh aux bornes et traitement automatisé
###############################################################################
# Author: Daniel STAN <daniel.stan@crans.org>
# Copyright (C) 2012
# All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
###############################################################################

import subprocess
import select
import threading
import time

import sys
if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')
from gestion import affich_tools

# Very verbose output
DEBUG = False
if "--debug" in sys.argv:
    DEBUG = True

SSH_KEY = '/etc/crans/secrets/id_rsa_wifi'

# Paramètres de connexions ssh
SSH_OPT=(
 ('StrictHostKeyChecking', 'no'),
 ('BatchMode', 'yes'),
 ('User', 'root'),
 # !!! /usr/scripts/gestion/clef-wifi deprecated
 ('IdentityFile', SSH_KEY),
 ('IdentityFile', '/usr/scripts/gestion/clef-wifi'),
 ('ConnectTimeout', '10'),   # 10 ? ssh très lent sur les bornes :/
 ('ForwardX11', 'no'),
 ('PasswordAuthentication', 'no'),
 #: Indiquer ici un fichier de known_hosts
 ('UserKnownHostsFile', '~/.ssh/ssh_known_bornes'),
)


def command_line(host, cmdtodo):
    """Génère le tableau des arguments de Popen"""
    if not type(host) in [str,unicode]:
        host = host['host'][0].value
    cmd = ['ssh', host,"-T"]
    for opt in SSH_OPT:
        cmd.append('-o')
        cmd.append('%s=%s' % opt)

    cmd.append(cmdtodo)
    return cmd

def exec_streams(host,cmdtodo):
    """Execute une commande en ssh sur une machine et renvoie sterr et stdout"""
    f = subprocess.Popen(command_line(host,cmdtodo),stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)

    return (f.stdout,f.stderr)

def exec_string(host,cmdtodo):
    """ Execute une commande et renvoie la chaîne de retour """
    (out,err) = exec_streams(host,cmdtodo)
    err.close()
    return out.read()

def exec_list_old(todo,cmd,done):
    """
        Déprécié
        Tente d'éxécuter la commande cmd sur la liste des todo
        si cela réussit, l'host est placé dans le dictionnaire done:
        dans ce dictionnaire, chaque host est associé à son output.
        Renvoie le dictionnaire des erreurs """
    new_todo = []
    todo.reverse()
    errors = {}
    while todo <> []:
        host=todo.pop()
        print(u"Début de connexion à %s..." % host)
        (out,err) = exec_streams(host,cmd)
        strerr = err.read()
        if len(strerr) > 0:
            print(u"--> Échec")
            new_todo.append(host)
            errors[host] = strerr
            out.close()
        else:
            print(u"--> Succès !")
            done[host] = out.read()
    todo+=new_todo
    return errors

def exec_list(hosts,cmd):
    """Déprécié.
    Paraléllisation d'une commande shell (cmd) sur plusieurs hôtes
    Renvoie un dictionnaire à trois entrées: 'out', 'err', 'ret'.
    Chaque valeur est un dictionnaire qui associe à un hôte (une des valeurs
    de hosts), sa sortie standard (respectivement erreur et code retour.)
    """
    
    procbyout = {}
    procbyerr = {}
    hostbyproc ={}
   
    graph = affich_tools.anim('SSH massif',2*len(hosts))

    outs = {'err':{},'out':{},'ret':{}}

    for host in hosts:
        proc = subprocess.Popen(command_line(host,cmd),stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        hostbyproc[proc] = host
        procbyout[proc.stdout] = proc
        procbyerr[proc.stderr] = proc
    
    listen = procbyout.keys() + procbyerr.keys()
    while listen <> []:
        (rlist,_,_) = select.select(listen,[],[],120)
        if rlist == []:
            raise Exception("Timeout !")
        for sock in rlist:
            if procbyout.has_key(sock):
                proc = procbyout[sock]
                typ = 'out'
            else:
                proc = procbyerr[sock]
                typ = 'err'
            host = hostbyproc[proc]
            read = sock.read()
            
            if not outs[typ].has_key(host):
                outs[typ][host] = ''
            outs[typ][host] += read
            
            ret = proc.poll()
            if ret == None:
                print('Process non fini...')
            else:
                outs['ret'][host] = ret
                graph.cycle()
                listen.remove(sock)
    return outs

def gen_config(liste):
    """ Génère un morceau de fichier .ssh/config pour se connecter
    facilement aux bornes """
    out = '#<Bornes wifi>\n'
    for b in liste:
        out += "Host %(b)s %(b)s.wifi %(b)s.wifi.crans.org\n" % {'b':b}
        out += "\tHostname %s.wifi.crans.org\n" % b
        for opt in SSH_OPT:
            out += '\t%s %s\n' % opt
    out += '#</Bornes wifi>\n'
    return out

def sh_list(hosts, script, cmd="/bin/sh", silent=False, timeout=-1):
    """Paraléllisation d'une commande shell (``cmd``) sur plusieurs hôtes,
    en envoyant un certain contenu (``script``).
    Renvoie un dictionnaire à trois entrées: ``'out'``, ``'err'``, ``'ret'``.
    Chaque valeur est un dictionnaire qui associe à un hôte (une des valeurs
    de ``hosts``), respectivement sa sortie standard, d'erreur et son code de retour."""
    # On ne veut pas avoir de doublons dans les hosts
    hosts = set(hosts)
    if DEBUG:
        print hosts
    
    # Dico qui récupérera toutes les sorties
    outs = {'err':{},'out':{},'ret':{}}
    
    if not silent:
        graph = affich_tools.anim('SSH massif',2*len(hosts))
    
    # On définit la fonction qui fera la même chose dans chacun des threads
    def execute(host, proc, his_script):
        try:
            if DEBUG:
                print u"Communicating %s …" % host
            out, err = proc.communicate(input=his_script)
            if DEBUG:
                print u"Communicating %s done" % host
        except EnvironmentError:
            return None
        if DEBUG:
            print "%s : %s" % (host, proc.poll())
        outs['out'][host], outs['err'][host], outs['ret'][host] = out, err, proc.poll()
        if not silent:
            graph.cycle()
    
    # On parallélise
    threads, procs = {}, {}
    for host in hosts:
        proc = subprocess.Popen(command_line(host,cmd), stdout=subprocess.PIPE,
            stderr=subprocess.PIPE, stdin=subprocess.PIPE)
        t = threading.Thread(target=execute, kwargs={"host" : host, "proc" : proc, "his_script" : script})
        threads[host] = t
        procs[host] = proc
        t.start()
    
    ## On s'occupe ensuite d'implémenter le timeout
    if timeout < 0:
        # Par défaut, il n'y en a pas, on va donc vérifier que tous les process ont terminé
        # ce n'est pas grave de le faire dans l'ordre, Thread.join retourne immédiatement si le thread a terminé
        for host, thread in threads.iteritems():
            thread.join()
    else:
        # On prend un top chrono
        begin = time.time()
        # On vérifie que les process finissent, mais sans dépasser le timeout total
        timeout_time = begin + timeout
        for host, thread in threads.iteritems():
            timeout_left = timeout_time - time.time()
            thread.join(timeout_left) # NB : Thread.join() d'une valeur négative retourne immédiatement
        # On tue les threads encore en vie
        for host, thread in threads.iteritems():
            if thread.is_alive():
                if not silent or DEBUG:
                    print "Killing process for %s." % host
                procs[host].terminate()
                thread.join()
    return outs

